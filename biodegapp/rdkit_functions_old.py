import pandas as pd 
import numpy  
import math
import rdkit
from rdkit import Chem, DataStructs
from rdkit.Chem import AllChem
from rdkit.Chem import rdMolDescriptors as rdmd
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import Draw
from rdkit.DataManip import Metric
from matplotlib.colors import ColorConverter
 
# fingerprint dictionaries
fp_dict = {}
fp_dict['morgan2'] = lambda m: rdmd.GetMorganFingerprintAsBitVect(m, 2, nBits=1024)
fp_dict['ap'] = lambda m: rdmd.GetHashedAtomPairFingerprintAsBitVect(m, nBits=2048)
fp_dict['rdk5'] = lambda m: Chem.RDKFingerprint(m, maxPath=5, fpSize=2048, nBitsPerHash=2)

# all available 2D descriptors
desc_names = [desc[0] for desc in Descriptors.descList]

# return array of fingerprint bits
def getNumpyFP(smiles, fpname, fptype):
    m = Chem.MolFromSmiles(smiles)
    if m is not None:
        # calculate fingerprint
        fp = fp_dict[fpname](m)
        # convert to numpy array
        if fptype == 'bool':
            arr = numpy.zeros((1,), numpy.bool)
        elif fptype == 'float':
            arr = numpy.zeros((1,), numpy.float32)
        else:
            raise ValueError('fptype not known', fptype)
        DataStructs.ConvertToNumpyArray(fp, arr)
        return arr
    else:
        return None
    
def extractRDKIT(mol_names, Smiles, fptype):
  mols = []
  fp_names = ['fp'+str(x) for x in range(1,len(getNumpyFP(Smiles[0], fptype, 'float'))+1)]
  for mol_name, smiles in zip(mol_names, Smiles):
      #print(mol_name, smiles)
      mol = Chem.MolFromSmiles(smiles)
      if mol is None:
            print(mol_name, smiles)
            continue
      fingerprint = getNumpyFP(smiles, fptype, 'float') #array
      #print(len(fingerprint),fingerprint)
      descriptors = [desc[1](mol) for desc in Descriptors.descList]
      #print(descriptors)
      #print(Descriptors.descList)
      descriptors = numpy.asarray(descriptors) #array
      
      #From list to data frame
      data_row = [mol_name]
      data_row.extend([smiles])
      data_row.extend(fingerprint)
      data_row.extend(descriptors)
      #print(data_row)
      mols.append(data_row) #one record
    
  #print(mols)    
  col_names = ['Molecule']
  col_names.extend(['SMILES'])
  col_names.extend(fp_names)
  col_names.extend(desc_names)

  features_df = pd.DataFrame.from_records(mols, columns=col_names)
  
  return features_df

def calcSimilarity(smi1, smi2):
  fp1 = fp_dict['rdk5'](Chem.MolFromSmiles(smi1))
  fp2 = fp_dict['rdk5'](Chem.MolFromSmiles(smi2))
  return DataStructs.FingerprintSimilarity(fp1,fp2, metric=DataStructs.TanimotoSimilarity)

def calcSimMatrix(smiles):
  fps = [ fp_dict['rdk5'](Chem.MolFromSmiles(smi)) for smi in smiles]
  sim_array = Metric.GetTanimotoSimMat(fps)
  #print(sim_array.shape)
  sim_mtx = numpy.zeros((len(smiles),len(smiles)))
  sim_mtx[numpy.tril_indices(len(smiles), -1)] = sim_array
  sim_mtx[numpy.triu_indices(len(smiles), 1)] = sim_array
  #print(sim_mtx.shape)
  #print(sim_mtx)
  return sim_mtx
  
def drawMolImages(smiles, molName):
  img = Draw.MolToImage(Chem.MolFromSmiles(smiles), 
                        highlightColor=ColorConverter().to_rgb('aqua'))
  img.save("./www/"+molName+".png")


  
