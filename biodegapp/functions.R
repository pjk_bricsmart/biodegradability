require(rcdk)
require(caret)
require(parallel)
require(doSNOW)
require(doParallel)

check.smiles <- function(smiles){
  as.logical(unlist(lapply(smiles, is.null)))
}

saveMolImg <- function(smiles, depictor, img_name){
  copy.image.to.clipboard(smiles, depictor)
  img <- view.image.2d(smiles, depictor)
  plot(c(100, 250), c(100, 250), axes = F, type = "n", xlab = "", ylab = "")
  rasterImage(img, 100, 100, 250, 250,
              interpolate = FALSE)
  dev.copy(png,img_name)
  dev.off()
}

prepareFeatures <- function(smiles){
  
  # DO NOT REMOVE MISSING FEATURES HERE. JUST ASSIGN NA TO BE 
  # CONSISTENT WITH OTHER PROCESSED BATCHES
  fp.types <- c("extended")#, "standard", "maccs", "kr", "circular")
  fingerp.df.all <- NULL
  for (fp.type in fp.types){
    fingerp.df <- lapply(smiles, get.fingerprint, type=fp.type, size=1024*2,depth=6)#optimized through clustering later
    #null.fp <- which(sapply(fingerp.df, is.null))
    
    fingerp.df <- lapply(fingerp.df, function(x) {
      fp.names <- paste(fp.type, sep="", seq(1:(x@nbit)))
      bits <- data.frame(matrix(0,nrow=1,ncol=x@nbit))
      names(bits) <- fp.names
      bits[,fp.names[x@bits]] <- 1
      bits
    })
    
    fingerp.df <- do.call("rbind", fingerp.df)
    
    if(fp.type==fp.types[[1]]){
      fingerp.df.all <- fingerp.df
    }else{
      fingerp.df.all <- cbind(fingerp.df.all,fingerp.df)
    }
  }
  
  fingerp.df <- fingerp.df.all
  
  desc.names <- get.desc.categories()
  desc.names <- sapply(desc.names, get.desc.names)
  
  descriptors.df <- lapply(smiles, FUN=function(x) {
    descr <- lapply(desc.names, FUN=function(y) tryCatch(eval.desc(x,y), error=function(e) NA))
    #descr <- descr[!vapply(descr, is.null, logical(1))]
    descr <- do.call("cbind", descr)
    descr
  })
  #desc.common <- lapply(descriptors.df,names)
  #desc.common <- Reduce(intersect, desc.common)
  #descriptors.df <- lapply(descriptors.df, FUN=function(x) x[,desc.common])
  desc.allnames <- unique(unlist(lapply(descriptors.df,names)))
  descriptors.df <- lapply(descriptors.df, FUN=function(x) {
    df <- data.frame(matrix(NA, nrow=nrow(x), ncol=length(desc.allnames)))
    names(df) <- desc.allnames
    for (nm in desc.allnames){
      if (nm%in%names(x)){
        df[,nm] <- x[,nm]
      }
    }
    df
  })
  descriptors.df <- do.call("rbind",descriptors.df)
  
  #descriptors.df <- transformFeatures(descriptors.df)
  
  features <- cbind(fingerp.df,descriptors.df)
  
  features
}

rmAllNA <- function(features.in){
  features <- features.in
  feat_rm <- apply(features, 2,FUN=function(x) all(is.na(x)|is.nan(x)|is.infinite(x)))
  features[,feat_rm] <- NULL
  features
}

transformFeatures <- function(features.in){
  
  features <- features.in
  
  #Add HAS.... features even if it's not necessary
  features.NA <- apply(features, 2, FUN=function(x) length(which(is.na(x)|is.nan(x)|is.infinite(x))))
  features.NA.nm <- names(features.NA)
  #features.NA <- apply(features, 2, FUN=function(x) any(is.na(x)|is.nan(x)|is.infinite(x)))
  #features.NA.nm <- names(features.NA)[features.NA]
  #print("To Transform:")
  #print(features.NA.nm)
  if(length(features.NA.nm)==0)
    return(features)
  features.na.transf <- NULL
  features.na.transf.1 <- NULL
  
  for (features.na in 1:length(features.NA)){
    features.na.nm <- features.NA.nm[features.na]
    features.na.nm.1 <- paste("HAS.",features.na.nm,sep="")
    features.na.value <- features[,features.na.nm]
    features[,features.na.nm.1] <- 1
    features[is.na(features.na.value)|
               is.nan(features.na.value)|
               is.infinite(features.na.value),
             features.na.nm.1] <- 0
    subst.value <- 0#mean(features[!is.na(features.na.value)&
                     #              !is.nan(features.na.value)&
                      #             !is.infinite(features.na.value),
                       #          features.na.nm])
    features[is.na(features.na.value)|
               is.nan(features.na.value)|
               is.infinite(features.na.value),
             features.na.nm] <- subst.value
    #print(paste(features.na.nm, "transformed"))
    features.na.transf <- c(features.na.transf, features.na.nm)
    features.na.transf.1 <- c(features.na.transf.1, features.na.nm.1)
  }
  #print(paste(length(features.na.transf), "features transformed"))
  
  features
}

processFeatures <- function(features.in, corr){
  
  features <- features.in
  #Rm columns with only 1 unique value
  features.nunique <- apply(features,2,FUN=function(x) length(unique(x))==1)
  features.nunique <- names(features.nunique)[features.nunique]
  if(length(features.nunique)>0){
    print("Non-unique features")
    print(features.nunique)
    features[,features.nunique] <- NULL
  }
  #Rm duplicated columns
  duplicated.columns <- duplicated(t(as.matrix(features)))
  duplicated.columns <- names(features)[duplicated.columns]
  if(length(duplicated.columns)){
    print("Duplicated features")
    print(duplicated.columns)
    features[,duplicated.columns] <- NULL
  }
  var.ZERO <- apply(features,2,FUN=function(x) var(x[!is.na(x)&
                                                       !is.nan(x)&
                                                       !is.infinite(x)])==0)
  var.ZERO <- names(var.ZERO)[var.ZERO]
  if(length(var.ZERO)>0){
    print("Var=0 features")
    print(var.ZERO)
    features[,var.ZERO] <- NULL
  }
  #Rm highly correlated features
  # Notice: use spearman method if data is skewed and otliers are supsected
  # Spearman is rank-based
  # Otherwise, use pearson
  if (corr){
    features_corrt <- apply(features,2,FUN=function(x){
      mean_x <- mean(x[!is.na(x)&
                         !is.nan(x)&
                         !is.infinite(x)])
      x[is.na(x)|
          is.nan(x)|
          is.infinite(x)] <- mean_x
      x
    })
    features.corr.mtx <- cor(features_corrt, method = "spearman")
    #findCorrelation(fingerp.corr.mtx, cutoff = 0.9, names=T)
    # If two variables have a high correlation, the function looks at the mean
    #absolute correlation of each variable and removes the variable with the largest
    #mean absolute correlation.
    toDrop <- findCorrelation(features.corr.mtx, cutoff = 0.75)
    if(length(toDrop)>0){
      print("High Correlation:")
      print(names(features)[toDrop])
      features <- features[,-toDrop]
    }
  }
  
  features
}


extractRCDK <- function(data.df, smiles.str = "SMILES", 
                        mol.len = 100, verbose=TRUE){
  
  cluster = makeCluster(parallel::detectCores() - 1) 
  registerDoSNOW(cluster)
  if(verbose)
    writeLines("", "./status.txt")
  
  data.df.1 <- foreach(mol.i=seq(1, nrow(data.df), by=mol.len), 
                       .packages = c("rcdk"), 
                       .export = c("check.smiles", "prepareFeatures", "transformFeatures"),
                       .combine=rbind, 
                       .errorhandling = "stop") %dopar% {
                         
                         if(verbose){
                           sink("./status.txt", append=T)
                           print(mol.i)
                         }
                         
                         mol.i.end <- (mol.i+mol.len-1)
                         if(nrow(data.df)-mol.i<=mol.len)
                           mol.i.end <- nrow(data.df)
                         mols.i <- c(mol.i:mol.i.end)#mol.i
                         
                         property.df <- data.df[mols.i,]
                         
                         smiles <- property.df[,smiles.str]
                         smiles <- parse.smiles(smiles)
                         
                         smiles2rm <- check.smiles(smiles)
                         property.df <- property.df[!smiles2rm,]
                         smiles <- smiles[!smiles2rm]
                         
                         if (length(smiles)==0){
                           next
                         }
                         
                         features <- prepareFeatures(smiles)
                         
                         data.df.tmp <- cbind(property.df[!smiles2rm,], features)
                         
                         if(verbose)
                           sink()
                         
                         data.df.tmp
                       }
  stopCluster(cluster)
  
  data.df.1
}

modelPerf <- function(data, pred.prob, threshold=NULL, 
                      positive_level="YES"){
  require(pROC)
  
  # find best threshold
  bestthresh <- NULL
  modelROC <- roc(response = data,
                  predictor = pred.prob[,positive_level],
                  positive = positive_level)
  auc <- modelROC$auc
  if(is.null(threshold)){
    bestthresh <- coords(modelROC, "best", ret = "threshold")
  }else{
    bestthresh  <- threshold
  }
  
  bestthresh <- bestthresh[1] #sometimes 2 thresholds are returned
  # calculate responses w/ this threshold
  pred.resp <- ifelse(pred.prob[,positive_level]>=bestthresh, "YES", "NO")

  # Confusion matrix for best threshold  
  actual = as.data.frame(table(data))
  names(actual) = c("Actual","ActualFreq")
  confusion = as.data.frame(table(data,
                                  pred.resp))
  names(confusion) = c("Actual","Predicted","Freq")
  #calculate percentage of test cases based on actual frequency
  confusion = merge(confusion, actual, by="Actual")
  confusion$Percent = confusion$Freq/confusion$ActualFreq*100
  tile <- ggplot() +
    geom_tile(aes(x=Actual, y=Predicted,fill=Percent),
              data=confusion, color="black",size=0.1) +
    labs(x="Actual",y="Predicted")
  tile = tile +
    geom_text(aes(x=Actual,y=Predicted, label=sprintf("%.1f", Percent)),
              data=confusion, size=10, colour="black") +
    scale_fill_gradient(low="grey",high="red")
  # lastly we draw diagonal tiles. We use alpha = 0 so as not to hide 
  # previous layers but use size=0.3 to highlight border
  tile = tile + 
    geom_tile(aes(x=Actual,y=Predicted),
              data=subset(confusion, as.character(Actual)==as.character(Predicted)),
              color="black",size=0.3, fill="black", alpha=0) 
  
  
  list(tile, auc, bestthresh, confusion, modelROC, pred.resp)
}

modelPred <- function(features, response, predictors, model, modelType){
  
  if (modelType=="BART"){
    pred <- bart_predict_for_test_data(model, #prob_rule_class = 0.6,  
                                       Xtest = features, 
                                       ytest = factor(rep("YES",nrow(features)), 
                                                      levels=c("YES","NO")))
    prob.biodeg <- round(1-pred$p_hat,3)*100 #Probability for being biodegradable
  }
  
  if (modelType=="XGBOOST"){
    augment <- FALSE
    
    if(nrow(features)==1){
      features[2,] = features[1,]
      augment = TRUE
    }
    
    if(augment){
      features[,response] <- factor(sample(c("NO","YES"),replace = F, 
                                           size=nrow(features)))
    }else{
      features[,response] <- factor(sample(c("NO","YES"),replace = T, 
                                           size=nrow(features)))
    }
    
    modelFmla <- as.formula(paste(response,"~",paste(predictors,collapse="+"), "-1"))
    
    featuresX <- build.x(modelFmla, data=features, 
                         contrasts = F, sparse = T) 
    featuresY <- build.y(modelFmla, features) %>% 
      as.factor() %>%
      as.integer() - 1 #0=NO, 1=YES 
    
    features <- xgb.DMatrix(data = featuresX, label=featuresY) 
    
    pred <- predict(model, newdata  = features, ntreelimit=model$best_ntreelimit)
    prob.biodeg <- round(as.numeric(pred),3)*100
    if(augment){
      prob.biodeg <- prob.biodeg[1]
    }
  }
  
  if (modelType=="GLMNET"){
    augment = FALSE
    
    if(nrow(features)==1){
      features[2,] = features[1,]
      augment=TRUE
    }
    
    if(augment){
      features[,response] <- factor(sample(c("NO","YES"),replace = F, 
                                           size=nrow(features)))
    }else{
      features[,response] <- factor(sample(c("NO","YES"),replace = T, 
                                           size=nrow(features)))
    }
    
    modelFmla <- as.formula(paste(response,"~",paste(predictors,collapse="+"), "-1"))
    
    featuresX <- build.x(modelFmla, data=features, 
                         contrasts = F, sparse = T) 
    pred <- predict(model, newx = featuresX, 
                    s="lambda.min", type="response")#prob for yes
    prob.biodeg <- round(as.numeric(pred),3)*100
    if(augment){
      prob.biodeg <- prob.biodeg[1]
    }
    
  }
  
  predTable <- data.frame("PROB.BIODEG." = prob.biodeg)
  names(predTable) <- paste("PROB.BIODEG.", modelType,sep="")
  
  predTable
}

customMetric <- function(preds, dtrain) {
  labels <- xgboost::getinfo(dtrain, "label")
  elab <- as.numeric(labels)
  epreds <- as.numeric(preds)
  epreds <- ifelse(epreds<0, 0, epreds)
  err <- sqrt((sum((log10(1+epreds) - log10(1+elab))^2))/length(epreds))
  
  return(list(metric = "my_rmse", value = err))
}

#https://xgboost.readthedocs.io/en/latest/parameter.html
xgbooster <- function(xgSearchGrid, X_Train, X_Test, Y_Train, Y_Test,
                      weight = NULL, nfolds = 5,
                      metric="auc", customMetric = NULL,
                      metric_max = TRUE,
                      objective='binary:logistic',
                      booster='gbtree', stratified = TRUE,
                      seed=2018){
  
  require(xgboost)
  
  xgTrain <- xgb.DMatrix(data = X_Train, label=Y_Train) 
  xgTest <- xgb.DMatrix(data = X_Test, label=Y_Test) 
  
  model <- NULL
  model_selected <- NULL
  best_nrounds <- NULL
  best <- NULL
  if(metric_max){
    best=-9999999999
  }else{
    best=9999999999
  }
  
  # Find best parameters through cross-validation on training set
  modelFit_xgb_tune_tr <- list()
  modelFit_xgb_tune_te <- list()
  modelFit_xgb_tune_stopped <- list()
  for (rr in 1:nrow(xgSearchGrid)){
    print(paste(rr, "of", nrow(xgSearchGrid)))
    print(xgSearchGrid[rr,])
    if (!is.null(customMetric)){
      set.seed(seed)
      modelFit_xgb <- xgb.cv(
        nrounds = ifelse(xgSearchGrid[rr,"eta"]==0.1,2000,10000),
        params = as.list(xgSearchGrid[rr,]),
        data = xgTrain,
        objective = objective,
        booster=booster,
        weight = weight,
        feval = customMetric,
        maximize = metric_max,
        stratified=stratified,
        nfold = nfolds,
        print_every_n = 500,
        watchlist=list(train=xgTrain, validate=xgTest),
        callbacks = list(cb.early.stop(stopping_rounds=70,
                                       metric_name = paste("test_", metric, sep=""),  
                                       maximize = metric_max))
      )
    }else{
      set.seed(seed)
      modelFit_xgb <- xgb.cv(
        nrounds = ifelse(xgSearchGrid[rr,"eta"]==0.1,2000,10000),
        params = as.list(xgSearchGrid[rr,]),
        data = xgTrain,
        objective = objective,
        booster=booster,
        weight = weight,
        stratified=stratified,
        eval_metric= metric,
        nfold = nfolds,
        print_every_n = 500,
        watchlist=list(train=xgTrain, validate=xgTest),
        callbacks = list(cb.early.stop(stopping_rounds=70,
                                       metric_name = paste("test_", metric, sep=""),  
                                       maximize = metric_max))
      )
    }
    modelFit_xgb_tune_te[[rr]] <- as.numeric(modelFit_xgb$evaluation_log[modelFit_xgb$best_ntreelimit, c(4)])
    modelFit_xgb_tune_tr[[rr]] <- as.numeric(modelFit_xgb$evaluation_log[modelFit_xgb$best_ntreelimit, c(2)])
    
    if (metric_max){
      if(modelFit_xgb_tune_te[[rr]]>best){
        model <- modelFit_xgb
        best <- modelFit_xgb_tune_te[[rr]]
        model_selected <- rr
        best_nrounds <- modelFit_xgb$best_iteration
      }
    }else{
      if(modelFit_xgb_tune_te[[rr]]<best){
        model <- modelFit_xgb
        best <- modelFit_xgb_tune_te[[rr]]
        model_selected <- rr
        best_nrounds <- modelFit_xgb$best_iteration
      }
    }
  }
  
  #x-validated model with parameters yielding best avg performance
  #over hould-out test sets
  model1 = model 
  
  # Train with best hyperparameters. Set nrounds based on previous
  # Validate on test set
  if(!is.null(customMetric)){
    set.seed(seed)
    model <- xgb.train(
      data = xgTrain, 
      objective = objective, 
      booster=booster,
      feval = customMetric,
      maximize = metric_max,
      nrounds = best_nrounds, 
      params = as.list(xgSearchGrid[rr,]),
      print_every_n = 500,
      weight = weight,
      stratified = stratified,
      watchlist=list(train=xgTrain, validate=xgTest),
      callbacks = list(cb.early.stop(stopping_rounds=50,
                                     metric_name = paste("validate_", metric, sep=""),  
                                     maximize = metric_max))
    )
  }else{
    set.seed(seed)
    model <- xgb.train(
      data = xgTrain, 
      objective = objective, 
      booster=booster,
      eval_metric=metric,
      nrounds = best_nrounds, 
      params = as.list(xgSearchGrid[rr,]),
      print_every_n = 500,
      weight = weight,
      stratified = stratified,
      watchlist=list(train=xgTrain, validate=xgTest),
      callbacks = list(cb.early.stop(stopping_rounds=50,
                                     metric_name = paste("validate_", metric, sep=""),  
                                     maximize = metric_max))
    )
  }
  
  model2 = model
  
  list(model1, model2, modelFit_xgb_tune_te, modelFit_xgb_tune_tr, model_selected)
}



keras_autoencoder <- function(x_train, y_train, x_test, y_test,
                              activation = "tanh", 
                              layer1_nodes = 15, layer2_nodes = 10, 
                              layer3_nodes = 15,
                              batch_size=32, epochs=10){
  #https://keras.rstudio.com/
  # devtools::install_github("rstudio/reticulate")
  # devtools::install_github("rstudio/tfruns")
  # devtools::install_github("rstudio/tensorflow")
  # devtools::install_github("rstudio/keras")
  require(keras)
  # the following command creates a conda env,called r-tensorflow (you can see it in anaconda)
  # (activate r-tensorflow, deactivate) so that we can interface to it
  # with r through keras r and reticulate
  #install_keras() #see function inputs for customized installation
  
  
  #layer_dropout?
  
  model <- keras_model_sequential()
  model %>%
    layer_dense(units = layer1_nodes, activation = activation, 
                input_shape = ncol(x_train)) %>%
    layer_dense(units = layer2_nodes, activation = activation) %>%
    layer_dense(units = layer3_nodes, activation = activation) %>%
    layer_dense(units = ncol(x_train))
  
  print(summary(model))
  
  model %>% compile(
    loss = keras::loss_mean_squared_error,
    metrics = keras::metric_mean_squared_error,
    optimizer = keras::optimizer_adadelta
  )
  
  earlystop_callback <- callback_early_stopping(mode="min", monitor = "val_mean_squared_error", patience = 5)
  
  history %>% fit(
    x=x_train, y=y_train, 
    validation_data = list(x_test,y_test),
    epochs = epochs, batch_size = batch_size, 
    callbacks = list(earlystop_callback)
  )
  
  #Final loss
  loss <- evaluate(model, x = x_test, y = x_test)
  
  plot(history)
  
}