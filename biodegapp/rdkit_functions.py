import pandas as pd 
import numpy  
import math
import rdkit
from rdkit import Chem, DataStructs
from rdkit.Chem import AllChem
from rdkit.Chem import rdMolDescriptors as rdmd
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import Draw
from rdkit.DataManip import Metric
from matplotlib.colors import ColorConverter
 
# fingerprint dictionaries
# Look here for completeness
fp_dict = {}
fp_dict['morgan2'] = lambda m, length: rdmd.GetMorganFingerprintAsBitVect(m, 2, nBits=length)
fp_dict['rdk2'] = lambda m, length: Chem.RDKFingerprint(m, maxPath=2, fpSize=length)
fp_dict['rdk4'] = lambda m, length: Chem.RDKFingerprint(m, maxPath=4, fpSize=length)
fp_dict['rdk5'] = lambda m, length: Chem.RDKFingerprint(m, maxPath=5, fpSize=length)
fp_dict['rdk6'] = lambda m, length: Chem.RDKFingerprint(m, maxPath=6, fpSize=length)
fp_dict['rdk7'] = lambda m, length: Chem.RDKFingerprint(m, maxPath=7, fpSize=length)
fp_dict['hasheda'] = lambda m, length: rdmd.GetHashedAtomPairFingerprintAsBitVect(m, nBits=length)
fp_dict['hashedt'] = lambda m, length: rdmd.GetHashedTopologicalTorsionFingerprintAsBitVect(m, nBits=length)

# all available 2D descriptors
desc_names = [desc[0] for desc in Descriptors.descList]

# return array of fingerprint bits
def getNumpyFP(smiles, fpname, fptype, nBits):
    m = Chem.MolFromSmiles(smiles)
    if m is not None:
        # calculate fingerprint
        fp = fp_dict[fpname](m, nBits)
        # convert to numpy array
        if fptype == 'bool':
            arr = numpy.zeros((1,), numpy.bool)
        elif fptype == 'float':
            arr = numpy.zeros((1,), numpy.float32)
        else:
            raise ValueError('fptype not known', fptype)
        DataStructs.ConvertToNumpyArray(fp, arr)
        return arr
    else:
        return None
    
def extractRDKIT(mol_names, Smiles, fptype, nBits=2048):
  mols = []
  fp_names = ['fp'+str(x) for x in range(1,len(getNumpyFP(Smiles[0], fptype, 'float', nBits))+1)]
  for mol_name, smiles in zip(mol_names, Smiles):
      #print(mol_name, smiles)
      mol = Chem.MolFromSmiles(smiles)
      if mol is None:
            print(mol_name, smiles)
            continue
      fingerprint = getNumpyFP(smiles, fptype, 'float', nBits) #array
      #print(len(fingerprint),fingerprint)
      descriptors = [desc[1](mol) for desc in Descriptors.descList]
      #print(descriptors)
      #print(Descriptors.descList)
      descriptors = numpy.asarray(descriptors) #array
      
      #From list to data frame
      data_row = [mol_name]
      data_row.extend([smiles])
      data_row.extend(fingerprint)
      data_row.extend(descriptors)
      #print(data_row)
      mols.append(data_row) #one record
    
  #print(mols)    
  col_names = ['Molecule']
  col_names.extend(['SMILES'])
  col_names.extend(fp_names)
  col_names.extend(desc_names)

  features_df = pd.DataFrame.from_records(mols, columns=col_names)
  
  return features_df

def extractFP(mol_names, Smiles, fptype, nBits=2048):
  mols = []
  fp_names = ['fp'+str(x) for x in range(1,len(getNumpyFP(Smiles[0], fptype, 'float', nBits))+1)]
  for mol_name, smiles in zip(mol_names, Smiles):
      #print(mol_name, smiles)
      mol = Chem.MolFromSmiles(smiles)
      if mol is None:
            print(mol_name, smiles)
            continue
      fingerprint = getNumpyFP(smiles, fptype, 'float', nBits) #array
      #From list to data frame
      data_row = [mol_name]
      data_row.extend([smiles])
      data_row.extend(fingerprint)
      #print(data_row)
      mols.append(data_row) #one record
    
  #print(mols)    
  col_names = ['Molecule']
  col_names.extend(['SMILES'])
  col_names.extend(fp_names)

  features_df = pd.DataFrame.from_records(mols, columns=col_names)
  
  return features_df

def calcSimilarity(smi1, smi2):
  fp1 = fp_dict['rdk5'](Chem.MolFromSmiles(smi1))
  fp2 = fp_dict['rdk5'](Chem.MolFromSmiles(smi2))
  return DataStructs.FingerprintSimilarity(fp1,fp2, metric=DataStructs.TanimotoSimilarity)

def calcSimMatrix(smiles):
  fps = [ fp_dict['rdk5'](Chem.MolFromSmiles(smi)) for smi in smiles]
  sim_array = Metric.GetTanimotoSimMat(fps)
  #print(sim_array.shape)
  sim_mtx = numpy.zeros((len(smiles),len(smiles)))
  sim_mtx[numpy.tril_indices(len(smiles), -1)] = sim_array
  sim_mtx[numpy.triu_indices(len(smiles), 1)] = sim_array
  #print(sim_mtx.shape)
  #print(sim_mtx)
  return sim_mtx
  
def drawMolImages(smiles, molName, folder="./www/"):
  img = Draw.MolToImage(Chem.MolFromSmiles(smiles), 
                        highlightColor=ColorConverter().to_rgb('aqua'))
  img.save(folder+molName+".png")


def extractRDKIT1(mol_name, filename, fptype, nBits=2048):
  mols = []

  mol = Chem.MolFromMolFile(filename)
  if mol is None:
    print(mol_name, smiles)
    return None
  
  smiles = Chem.MolToSmiles(mol)
  fp_names = ['fp'+str(x) for x in range(1,len(getNumpyFP(smiles, fptype, 'float', nBits))+1)]

  fingerprint = getNumpyFP(smiles, fptype, 'float', nBits) #array
  #print(len(fingerprint),fingerprint)
  descriptors = [desc[1](mol) for desc in Descriptors.descList]
  #print(descriptors)
  #print(Descriptors.descList)
  descriptors = numpy.asarray(descriptors) #array
      
  #From list to data frame
  data_row = [mol_name]
  data_row.extend([smiles])
  data_row.extend(fingerprint)
  data_row.extend(descriptors)
  #print(data_row)
  mols.append(data_row) #one record
    
  #print(mols)    
  col_names = ['Molecule']
  col_names.extend(['SMILES'])
  col_names.extend(fp_names)
  col_names.extend(desc_names)

  features_df = pd.DataFrame.from_records(mols, columns=col_names)
  
  return features_df

