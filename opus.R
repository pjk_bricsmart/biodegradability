### Biodegradability >> opus.R
### P J Kowalczyk

# libraries
if (!require("pacman")) install.packages("pacman")
pacman::p_load(readxl, lubridate, checkmate, Hmisc, corrplot,
               psych, tidyverse, magrittr, GGally, rpart,
               ggplot2, ggthemes, randomForest,
               dplyr, visdat, e1071, stats, ggparallel,
               gridExtra, stringr, xray, dataMaid, janitor, scales, stats,
               changepoint, caret)

Cheng_train_test <- read_xls('data/ChengData.xls', sheet = 'Training set and Test Set') %>%
  select(-`Appliation Domain (ID: In Domain, OD: Out of Domain)`) %>%
  janitor::clean_names() %>%
  rename(endpoint = experimental_labels) %>%
  rename(set = data_set)

Cheng_train_test$set <- ifelse(Cheng_train_test$set == 'Train set', 'train', 'test')

Cheng_BOD <- read_xls('data/ChengData.xls', sheet = '817 compounds with BOD% Value') %>%
  janitor::clean_names()
